<?php
require('./banco-dados/credentials.php');
session_start();
$usuario_logado = $_SESSION['usuario'];
$nome = $email = $senha = $pontuacao = $historico = "";

if (isset($_SESSION['usuario'])) {
    $conn = mysqli_connect($servername, $username, $password, $dbname);
    if (!$conn) {
        die("Error connecting to the database: " . mysqli_connect_error());
    }

    $usuario_logado = mysqli_real_escape_string($conn, $usuario_logado); 

    $sql = "SELECT idusuario, nome,  email, senha, pontuacaousuario FROM usuario WHERE usuario.nome='$usuario_logado'or usuario.email='$usuario_logado'";
    $resultado = mysqli_query($conn, $sql);

    if ($resultado && mysqli_num_rows($resultado) > 0) {
        $row = mysqli_fetch_assoc($resultado);
        $idusuario = $row['idusuario'];
        $nome = $row['nome'];
        $email = $row['email'];
        $senha = $row['senha'];
        $pontuacaousuario = $row['pontuacaousuario'];

        if ($usuario_logado !== $nome && $usuario_logado !== $email) {
            header("Location: login.php");
            exit();
        }
    }

    $sqlTabelaHistorico = "SELECT historico.idpartida, partida.pontuacao, partida.data
                            FROM historico
                            INNER JOIN partida ON historico.idpartida = partida.idpartida
                            WHERE historico.idusuario = '$idusuario';";
$result = mysqli_query($conn, $sqlTabelaHistorico);

$sqlTabelaHistorico = mysqli_query($conn, $sqlTabelaHistorico);
$idpartida = isset($row['idpartida']) ? $row['idpartida'] : '';
$pontuacao = isset($row['pontuacao']) ? $row['pontuacao'] : '';
$data = isset($row['data']) ? $row['data'] : '';
    $row = mysqli_fetch_assoc($sqlTabelaHistorico);
   

    mysqli_close($conn);
}
?>


<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Perfil</title>
    <link rel="stylesheet" href="../css/perfil.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">

</head>

<body>
    <div class="d-flex align-items-center" style="padding: 20px; padding-left: 80px;" id="top">
        <button class="botao_inicial" type="button" onclick="window.location.href='home.php'">
            <img src="../imgs/duck.png" style="width: 50px; height: 50px; margin-right: 10px" alt="Home" />
        </button>
        <h1 class="display-6 w-100">
            <span class="nome green">duck</span><span class="nome orange" style="color: orange">type</span>
        </h1>
        <div class="sair">
            <button type="button" class="bbotao" onclick="window.location.href='logout.php'" style="border-color: #40C057;color: green; background-color: #ffffff; border-radius: 5px; margin: 3%;">Sair</button>
        </div>
    </div>

    <div id="base">
        <div id="perfil">
            <h1 id="titulo">PERFIL</h1>
            <p class="titulos">Nome de Usuário</p>
            <?php echo '<p class="phps">' . $nome . '</p>' ?>
            <p class="titulos">Email</p>
            <?php echo '<p class="phps">' . $email . '</p>' ?>
           
            <p class="titulos">Pontuação geral</p>
    <?php echo '<p class="phps">' . ($pontuacaousuario ? $pontuacaousuario : 'N/A') . '</p>' ?>

    <p class="titulos">Histórico de Partidas</p>
    <?php 
    if ($result && mysqli_num_rows($result) > 0) {
        echo "<table class='custom-table'>";
        echo "<tr>
                <th>Id da Partida</th>
                <th>Pontuação</th>
                <th>Data</th>
              </tr>";

        while ($row = mysqli_fetch_assoc($result)) {
            echo "<tr>";
            echo "<td>" . $row['idpartida'] . "</td>";
            echo "<td>" . $row['pontuacao'] . "</td>";
            echo "<td>" . $row['data'] . "</td>";
            echo "</tr>";
        }

        echo "</table>";
    } else {
        echo "<p class='phps'>Nenhum resultado encontrado.</p>";
    }
    ?>
</div>

        <div>
            <img class="imagem" src="../imgs/profileduck.gif" />
        </div>
    </div>
</body>

</html>