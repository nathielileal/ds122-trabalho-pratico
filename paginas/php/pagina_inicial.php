  

    <!DOCTYPE html>
    <html lang="pt-br">
        <head>
            <meta charset="utf-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
            
            <link rel="stylesheet" href="../css/paginainicial.css">

            <link rel="icon" href="../imgs/duck.png" type="image/x-icon">
            <title>ducktype</title>
        </head>
        <body>

      
            <div id="centro"> <h1 class="ducktype">
                    <span class="nome green">duck</span><span class="nome orange">type</span>
                </h1>
                <p id="descricao">o jogo de digitação do patinho!</p>
                </div>
                <div class="logincadastro">
                    <button type="button" class="btn btn-outline-custom" onclick="window.location.href='login.php'" >Login</button>
                    <button type="button" class="btn btn-outline-custom" onclick="window.location.href='cadastro.php'">Criar conta</button>
                </div>
            </div>
        

            <footer class="footer">
                <p>Desenvolvido por Maria Tereza Frank e Nathieli Leal &copy; 2023</p>
            </footer>
                
           <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous"></script>

            
        </body>
    </html>
