<?php

function verifica_campo($conn, $texto){
  $texto = trim($texto);
  $texto = stripslashes($texto);
  $texto = htmlspecialchars($texto);
  $texto = mysqli_real_escape_string($conn, $texto);
  return $texto;
}

$nome = "";
$email = "";
$senha = "";

$nome = verifica_campo($conn, $_POST['form_cadastro_nome']);
$email = verifica_campo($conn, $_POST['form_cadastro_email']);
$senha = verifica_campo($conn, $_POST['form_cadastro_senha']);

?>
