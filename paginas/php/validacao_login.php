<?php

function verifica_campo($conn, $texto){
  $texto = trim($texto);
  $texto = stripslashes($texto);
  $texto = htmlspecialchars($texto);
  $texto = mysqli_real_escape_string($conn, $texto);
  return $texto;
}

$usuario = "";
$login_senha = "";

$usuario = verifica_campo($conn, $_POST['form-login-usuario']);
$login_senha = verifica_campo($conn, $_POST['form-login-senha']);

?>
