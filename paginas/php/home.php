<?php session_start();
if (isset($_SESSION['usuario'])) {
    $usuario_logado = $_SESSION['usuario'];
} else {
    header("Location: login.php");
    exit();
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Home</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css">
    <link rel="stylesheet" href="../css/home.css">
</head>
<body>
    <div class="d-flex align-items-center" style="padding: 20px; padding-left: 80px;" id="top">
        <h1 class="display-6 w-100">
            <span class="nome green">duck</span><span class="nome orange">type</span>
        </h1> <br>
    </div> 
    <p id="descricao">Home</p>

<div id="base">
    <div id="esquerda">
    <div class="blocos">
        <button class="botao_inicial" type="button" onclick="window.location.href='perfil.php'">
            <img class="perfil" src="../imgs/perfil.png"  alt="Perfil"/>
        </button> Perfil 
    </div>
    <div class="blocos">
        <button class="botao_inicial" type="button" onclick="window.location.href='classificacao.php'">
            <img class="perfil" src="../imgs/classificacao.png"  alt="Perfil"/>
        </button> Classificação
    </div> </div>
   <div id="direita">
    <div class="blocos">
        <button class="botao_inicial" type="button" onclick="window.location.href='ligas.php'">
            <img class="perfil" src="../imgs/aloligas.png"  alt="Perfil"/>
        </button> Ligas
    </div>
    <div class="blocos">
        <button class="botao_inicial" type="button" onclick="window.location.href='jogo.php'">
            <img class="perfil" src="../imgs/alvo.png"  alt="Perfil"/>
        </button> Jogo
    </div>
</div>
</div>
    
</body>
</html>