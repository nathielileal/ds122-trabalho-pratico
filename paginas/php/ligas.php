<?php 
    session_start();
        
    if (isset($_SESSION['usuario'])) {
        $usuario_logado = $_SESSION['usuario'];
        echo "alo, $usuario_logado!";
    } else {
        header("Location: login.php");
        exit();
    }

    require('../php/banco-dados/credentials.php');

    $conn = mysqli_connect($servername,$username,$password,$dbname);

    if(!$conn){
        die('problemas ao conectar com o banco de dados' . mysqli_connect_error());
    }

    $idliga = $_GET['idliga'] ?? $liga['idliga'] ?? null;

    if ($_SERVER["REQUEST_METHOD"] === "POST") {
        $nomeliga = mysqli_real_escape_string($conn, $_POST["form-nome-liga"]);
        $qtdemaxjogadores = mysqli_real_escape_string($conn, $_POST["form-qtde-max-jogadores"]);
        $palavrapasse = mysqli_real_escape_string($conn, $_POST["form-palavra-chave"]); 
        
        $sql = "INSERT INTO liga (nomeliga, quantidademaxjogadores, quantidadejogadores, palavrapasse) VALUES ('$nomeliga', '$qtdemaxjogadores', 0, '$palavrapasse')";
        
        if(!mysqli_query($conn, $sql)) {
            die('problemas ao adicionar liga' . mysqli_error($conn));
        } 

        if(isset($_POST["entrar"])) {
            $sql = "SELECT idliga, quantidademaxjogadores, quantidadejogadores, palavrapasse FROM liga WHERE nomeliga = '$nomeliga'";
            $entrar = mysqli_query($conn, $sql);

            if (!$entrar) {
                die('Problema ao acessar liga' . mysqli_error($conn));
            }

            if (mysqli_num_rows($entrar) > 0) {
                while ($liga = mysqli_fetch_assoc($entrar)) {
                    if ($palavrapasse == $liga['palavrapasse']) {
                        if ($liga['quantidadejogadores'] < $liga['quantidademaxjogadores']) {
                            $novaquantidade = $liga['quantidadejogadores'] + 1;
                            $idliga = $liga['idliga'];
                            $sql_update = "UPDATE liga SET quantidadejogadores = $novaquantidade WHERE idliga = $idliga";
                            mysqli_query($conn, $sql_update);

                            $sql_update_usuario = "UPDATE usuario SET idliga = $idliga WHERE nome =  $usuario_logado;";

                            mysqli_query($conn, $sql_update);
        
                            header("Location: jogo.php");
                            exit();
                        } else {
                            $mensagem_erro = "Liga cheia. Escolha outra liga.";
                        }
                    } else {
                        $mensagem_erro = "Senha incorreta. Verifique os dados e tente novamente.";
                    }
                }
            } else {
                $mensagem_erro = "Liga não encontrada. Verifique os dados e tente novamente.";
            }
        }
    } 

    $sql = "SELECT nomeliga, quantidademaxjogadores, quantidadejogadores FROM liga GROUP BY idliga ORDER BY idliga ASC;";
    $result = mysqli_query($conn,$sql);

    if(!$result){
        die('problemas ao mostrar ligas' . mysqli_error($conn));
    }
?>

    <!DOCTYPE html>
    <html lang="en">
        <head>
            <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">

            <title>Ligas</title>
            <link rel="icon" href="../imgs/duck.png" type="image/x-icon">

            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">

            <link rel="stylesheet" href="../css/liga.css">
            
        </head>
        <body>
            <div class="d-flex align-items-center" style="padding: 20px; padding-left: 80px;" id="top">
                <button class="botao_inicial" type="button" onclick="window.location.href='home.php'">
                    <img src="../imgs/duck.png" style="width: 50px; height: 50px; margin-right: 10px;" alt="Home"/>
                </button> 
                <h1 class="display-6 w-100">
                    <span class="nome green">duck</span><span class="nome orange">type</span>
                </h1>
            </div> 

            <div id="base">
                <div id="geral">
                    <p class="dica">Entre em uma liga</p>
                    <p class="explica">Insira a palavra-passe para participar!</p>
                    <hr class="linha">
                    <div class="lista">
                        <div id="container-liga">
                                <?php if(mysqli_num_rows($result) > 0): ?>
                                    <?php while($ligas = mysqli_fetch_assoc($result)): ?>
                                        <div class="conteudo-liga">
                                            <div class="informacoes">
                                                <p class="nome-liga"><?php echo $ligas["nomeliga"]?></p>
                                                <div class="qtde-jogadores">
                                                    <p class="jogadores">Jogadores:</p>
                                                    <p class="qtde"><?php echo $ligas["quantidadejogadores"] ?>/<?php echo $ligas["quantidademaxjogadores"] ?></p>
                                                </div>
                                            </div>
                                            <div class="d-flex align-items-center">
                                                <form class="col-md-9 mb-2 d-flex align-items-center" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="POST">
                                                    <input required type="text" class="input form-control" id="form-palavra-chave" name="form-palavra-chave" placeholder="Insira a palavra-passe para acesso" value="">
                                                    <input type="hidden" name="form-nome-liga" value="<?php echo $ligas["nomeliga"] ?>">
                                                    <input type="hidden" name="form-qtde-max-jogadores" value="<?php echo $ligas["quantidademaxjogadores"] ?>">
                                                    <div class="col-md-3 mb-2">
                                                        <button type="submit" class="entrar btn btn-outline-custom" id="entrar" name="entrar">Entrar</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    <?php endwhile; ?>
                                <?php else: ?>
                                    <p>nenhuma liga criada :(</p>
                                <?php endif; ?>
                        </div>
                        <?php if (isset($mensagem_erro)): ?>
                            <span class="help-block"><?php echo $mensagem_erro; ?></span>
                        <?php endif; ?>
                    </div>
                </div>
                
                <div id="criar-liga">
                    <form id="form-criar-liga" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="POST">
                        <p class="dica">Crie uma liga</p>
                        <p class="explica">Compartilhe a palavra-passe para jogar com os amigos!</p>
                        <hr class="linha">
                        <span id="msg-erro-cadastro"></span>
                            
                        <div class="col-md-11 mb-3 <?php if(!empty($erro_nome)){echo "has-error";}?>">
                            <label for="form-nome-liga" class="form-titulo col-form-label">Nome da liga:</label>
                            <input required type="text" class="input form-control" id="form-nome-liga" name="form-nome-liga" placeholder="Escolha um nome para sua liga" value="">
                            <div id="erro_nome">

                            </div>
                                <?php if (!empty($erro_nome)): ?>
                                    <span class="help-block"><?php echo $erro_nome ?></span>
                                <?php endIf; ?>
                        </div>
                            
                        <div class="col-md-11 mb-3 <?php if(!empty($erro_email)){echo "has-error";}?>">
                            <label for="form-qtde-max-jogadores" class="form-titulo col-form-label">Quantidade de jogadores:</label>
                            <input required type="number" id="form-qtde-max-jogadores" class="input form-control" name="form-qtde-max-jogadores" placeholder="0" value=""> 
                            <div id="erro_email">

                            </div>
                            <?php if (!empty($erro_email)): ?>
                                <span class="help-block"><?php echo $erro_email ?></span>
                            <?php endIf; ?>
                        </div>

                        <div class="col-md-11 mb-3 <?php if(!empty($erro_nome)){echo "has-error";}?>">
                            <label for="form-palavra-chave" class="form-titulo col-form-label">Palavra-chave para acesso:</label>
                            <input required type="text" class="input form-control" id="form-palavra-chave" name="form-palavra-chave" placeholder="Crie uma palavra-passe para acesso" value="">
                            <div id="erro_nome">

                            </div>
                            <?php if (!empty($erro_nome)): ?>
                                <span class="help-block"><?php echo $erro_nome ?></span>
                            <?php endIf; ?>
                        </div>
                            
                        <div class="col-md-12 mb-3">
                            <button type="submit" id="criar" class="criar btn btn-outline-custom" name="criar">Criar</button>
                        </div>
                    </form>
                </div>
            </div>

        </body>
    </html>