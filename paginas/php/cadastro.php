<?php
    require ('./banco-dados/conexao.php'); 
    require ('./banco-dados/credentials.php');
  
    $nome = $email = $senha = '';
    $erro_nome = $erro_email = $erro_senha = '';
    $conn = mysqli_connect($servername,$username,$password,$dbname);
?>
<?php
    if ($_SERVER["REQUEST_METHOD"] === "POST") {
        require("validacao_cadastro.php");

        $nome = mysqli_real_escape_string($conn, $_POST["form_cadastro_nome"]);
        $email = mysqli_real_escape_string($conn, $_POST["form_cadastro_email"]);
        $senha = mysqli_real_escape_string($conn, $_POST["form_cadastro_senha"]);

    $sql_verifica_nome = "SELECT * FROM usuario WHERE nome = '$nome'";
    $resultado_nome = mysqli_query($conn, $sql_verifica_nome);

    $sql_verifica_email = "SELECT * FROM usuario WHERE email = '$email'";
    $resultado_email = mysqli_query($conn, $sql_verifica_email);

    if (mysqli_num_rows($resultado_nome) > 0) {
        $erro_nome = "Nome de usuário já existe!";
    }

    if (mysqli_num_rows($resultado_email) > 0) {
        $erro_email = "E-mail já está em uso!";
    }

        if (empty($erro_nome) && empty($erro_email) && empty($erro_senha)) {
            $sql = "INSERT INTO usuario (nome, email, senha) VALUES ('$nome', '$email', '$senha')";

            if (mysqli_query($conn, $sql)) {
                echo "sucesso!";
                header("Location: login.php");
                exit();
            } else {
                echo "erro" . mysqli_error($conn);
            }
        }
    }

     ?>

                

<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">

        <link rel="stylesheet" href="../css/cadastro.css">

        <link rel="icon" href="../imgs/duck.png" type="image/x-icon">
        <title>ducktype</title>
    </head>
    <body>

        <div class="container-titulo d-flex align-items-center" >
            <button class="botao_inicial" type="button" onclick="window.location.href='pagina_inicial.php'">
                <img class="titulo-icon" src="../imgs/duck.png" />
            </button>
            <h1 class="display-6 w-100">
                <span class="nome green">duck</span><span class="nome orange">type</span>
            </h1>
        </div>

        <div class="conteudo">
            <div class="container-cadastro">
                <form id="form_cadastro" method="POST" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">
                    <p class="dica">Faça seu cadastro</p>
                    <hr class="linha">
                    <span id="msg-erro-cadastro"></span>
                    
                    <div class="col-md-12 mb-3 <?php if(!empty($erro_nome)){echo "has-error";}?>">
                        <label for="nome" class="form_titulo col-form-label">Nome de usuário:</label>
                        <input autocomplete="off" required type="text" class="input form-control" id="nome" name="form_cadastro_nome" placeholder="Nome de usuário" value="<?php echo $nome ?>">
                        <div id="erro_nome">

                        </div>
                        <?php if (!empty($erro_nome)): ?>
                            <span class="help-block"><?php echo $erro_nome ?></span>
                        <?php endIf; ?>
                    </div>
                    
                    <div class="col-md-12 mb-3 <?php if(!empty($erro_email)){echo "has-error";}?>">
                        <label for="email" class="form_titulo col-form-label">E-mail:</label>
                        <input autocomplete="off" required type="text" id="email" class="input form-control" name="form_cadastro_email" placeholder="name@example.com" value="<?php echo $email?>"> 
                        <div id="erro_email">

                        </div>
                        <?php if (!empty($erro_email)): ?>
                            <span class="help-block"><?php echo $erro_email ?></span>
                        <?php endIf; ?>
                    </div>
                    
                    <div class="col-md-12 mb-3 <?php if(!empty($erro_senha)){echo "has-error";}?>">
                        <label for="senha" class="form_titulo col-form-label">Senha:</label>
                        <input type="password" autocomplete="off"required type="text" id="senha" name="form_cadastro_senha" class="form-control" placeholder="Senha" value="<?php echo $senha ?>"> 
                        <div id="erro_senha">

                        </div>
                        <?php if (!empty($erro_senha)): ?>
                            <span class="help-block"><?php echo $erro_senha ?></span>
                        <?php endIf; ?>
                    </div>

                    <div class="col-md-12 mb-3">
                        <button type="submit" class="acessar btn btn-outline-custom">Acessar</button>
                    </div>
                </form> 
            </div>

            <div>
                <img class="imagem" src="../imgs/pato-andando.gif" />
            </div>
        </div>
    </body>
</html>