
<?php
    require('banco-dados/credentials.php'); 
    session_start();
    $usuario = $senha = $email = "";
    $erro_usuario = $erro_senha = $erro_email ="";
    $conn = mysqli_connect($servername,$username,$password,$dbname);

    if ($_SERVER["REQUEST_METHOD"] === "POST") {
        require("validacao_login.php");
        $usuario = $_POST["form-login-usuario"];
        $email = $_POST["form-login-usuario"];      
        $senha = $_POST["form-login-senha"];

        $query = "SELECT * FROM usuario WHERE nome = '$usuario' OR email = '$email' AND senha = '$senha'";
        $resultado = mysqli_query($conn, $query);

        $sql_verifica_nome = "SELECT * FROM usuario WHERE nome = '$usuario' OR email = '$usuario'";
        $resultado_usuario = mysqli_query($conn, $sql_verifica_nome);

        $sql_verifica_senha = "SELECT * FROM usuario WHERE senha = '$senha'";
        $resultado_senha = mysqli_query($conn, $sql_verifica_nome);

        if (mysqli_num_rows($resultado_usuario) < 1) {
            $erro_usuario = "Usuário não existe!";
            $usuario = "";
            $email = "";
        }

        if (mysqli_num_rows($resultado_senha) < 1) {
            $erro_senha = "Senha incorreta!";
            $senha = "";
        }


        if (mysqli_num_rows($resultado) > 0) {
            $_SESSION['usuario'] = $usuario; 
            header("Location: home.php");
            exit();
        } else {
            echo "<script>console.log('Credenciais inválidas');</script>";
        }
    }

?>

<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <link rel="icon" href="../imgs/duck.png" type="image/x-icon">
        <title>ducktype</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
        <link rel="stylesheet" href="../css/login.css">

        <script src="https://code.jquery.com/jquery-3.6.4.min.js"></script>
        <script src="../js/validacao.js"></script>


    </head>
    <body>
        <div class="container-titulo d-flex align-items-center" >
            <button class="botao_inicial" type="button" onclick="window.location.href='pagina_inicial.php'">
                <img class="titulo-icon" src="../imgs/duck.png" />
            </button>
            <h1 class="display-6 w-100">
                <span class="nome green">duck</span><span class="nome orange">type</span>
            </h1>
        </div>

        <div class="conteudo">
            <div class="container-cadastro">
                <form id="form-login" method="POST" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">
                    <p class="dica">Faça seu login</p>
                    <hr class="linha">
                    <span id="msg-erro-login" class="msg-erro-login"></span>
                    
                    <div class="col-md-12 mb-3 <?php if(!empty($erro_usuario) && !empty($erro_usuario)){echo "has-error";}?>">
                        <label for="form-login-usuario" class="form-titulo col-form-label" id="nomeusu">Nome de usuário ou e-mail:</label>
                        <input required type="text" name="form-login-usuario" class="input form-control" id="email" placeholder="Nome de usuário ou e-mail"  value="<?php echo isset($usuario) ? $usuario : $email; ?>">
                        <div id="erro_usuario"></div>
                        <?php if (!empty($erro_usuario && !empty($erro_usuario))): ?>
                            <span class="help-block"><?php echo isset($usuario) ? $erro_usuario : $erro_email; ?></span>
                        <?php endIf; ?>
                    </div>
                    
                            
                    <div class="col-md-12 mb-3 <?php if(!empty($erro_senha)){echo "has-error";}?>">
                        <label for="form-login-senha" class="form_titulo col-form-label">Senha:</label>
                        <br>
                        <input type="password" autocomplete="off" required type="text" name="form-login-senha" class="input form-control" id="senha" placeholder="Senha" value="<?php echo $senha ?>">
                        <div id="erro_login_senha"></div>
                        <?php if (!empty($erro_senha)): ?>
                            <span class="help-block"><?php echo $erro_senha ?></span>
                        <?php endIf; ?>
                    </div>
                    
                    <div class="col-md-12 mb-3">
                        <button type="submit" class="acessar btn btn-outline-custom">Acessar</button>
                    </div>
                </form> 
            </div>

            <div>
                <img class="imagem" src="../imgs/pato-andando.gif" />
            </div>
        </div>
    </body>
</html>

<!--combinar input com bd, true=login autorizado senao erro_login_senha ok-->
