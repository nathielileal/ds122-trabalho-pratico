<?php
require('./banco-dados/credentials.php');
session_start();
$usuario_logado = $_SESSION['usuario'];
$nome = $email = $senha = $pontuacao = $historico = "";

$nome = $usuario_logado;
if (isset($_SESSION['usuario'])) {
    $conn = mysqli_connect($servername, $username, $password, $dbname);
    if (!$conn) {
        die("Error connecting to the database: " . mysqli_connect_error());
    }
    $sql = "SELECT idusuario, nome,  email, senha, pontuacaousuario FROM usuario WHERE usuario.nome='$usuario_logado'";
    $resultado = mysqli_query($conn, $sql);

    if ($resultado && mysqli_num_rows($resultado) > 0) {
        $row = mysqli_fetch_assoc($resultado);
        $idusuario = $row['idusuario'];
        $row['nome'] = $_SESSION['usuario'];
        $nome = $row['nome'];
        $pontuacaousuario = $row['pontuacaousuario'];

        if ($usuario_logado !== $nome) {
            header("Location: login.php");
            exit();
        }
    }

    $sqlclassificacaogeral = "select idusuario, nome, pontuacaousuario from usuario where pontuacaousuario >0 order by pontuacaousuario  desc;";
$result = mysqli_query($conn, $sqlclassificacaogeral);

$sqlclassificacaogeral = mysqli_query($conn, $sqlclassificacaogeral);
    $row = mysqli_fetch_assoc($sqlclassificacaogeral);

    $sqlclassificacaosemanal = 
    "SELECT distinct usuario.idusuario, nome, pontuacaousuario, week(data) FROM usuario, partida
    WHERE WEEK(partida.data) = WEEK(NOW()) and pontuacaousuario >0
    ORDER BY pontuacaousuario DESC;";
    $resultsemanal = mysqli_query($conn, $sqlclassificacaosemanal);
    $sqlclassificacaosemanal = mysqli_query($conn, $sqlclassificacaosemanal);
    $rowsemanal = mysqli_fetch_assoc($sqlclassificacaosemanal);
    //$data = $rowsemanal['data'];
    mysqli_close($conn);
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Classificação</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
    <link rel="stylesheet" href="../css/classificacao.css">
</head>
<body>
<div class="d-flex align-items-center" style="padding: 20px; padding-left: 80px;" id="top">
        <button class="botao_inicial" type="button" onclick="window.location.href='home.php'">
            <img src="../imgs/duck.png" style="width: 50px; height: 50px; margin-right: 10px;" alt="Home"/>
        </button> 
        <h1 class="display-6 w-100">
            <span class="nome green">duck</span><span class="nome orange">type</span>
        </h1>
      </div> 

      <div id="base">
<div id="geral">
    <h1>CLASSIFICAÇÃO GERAL</h1>
    <?php 
    if ($result && mysqli_num_rows($result) > 0) {
        echo "<table class='custom-table'>";
        echo "<tr>
            <th>Id </th>
            <th>Nome do usuário</th>
                <th>Pontuação</th>
              </tr>";

        while ($row = mysqli_fetch_assoc($result)) {
            echo "<tr>";
            echo "<td>" . $row['idusuario'] . "</td>";
            echo "<td>" . $row['nome'] . "</td>";
            echo "<td>" . $row['pontuacaousuario'] . "</td>";
            echo "</tr>";
        }

        echo "</table>";
    } else {
        echo "<p class='phps'>Nenhum resultado encontrado.</p>";
    }
    ?>
</div>



<div id="semanal">
<h1>CLASSIFICAÇÃO SEMANAL</h1>
<?php 
    if ($resultsemanal && mysqli_num_rows($resultsemanal) > 0) {
        echo "<table class='custom-table'>";
        echo "<tr>
            <th>Id </th>
            <th>Nome do usuário</th>
                <th>Pontuação</th>
                <th>Semana</th>

              </tr>"; 

        while ($rowsemanal = mysqli_fetch_assoc($resultsemanal)) {
            echo "<tr>";
            echo "<td>" . $rowsemanal['idusuario'] . "</td>";
            echo "<td>" . $rowsemanal['nome'] . "</td>";
            echo "<td>" . $rowsemanal['pontuacaousuario'] . "</td>";
            echo "<td>" . $rowsemanal['week(data)'] . "</td>";

            echo "</tr>";
        }
        echo "</table>";
    } else {
        echo "<p class='phps'>Nenhum resultado encontrado.</p>";
    }
    ?>
</div>
        </div>
</body>
</html>