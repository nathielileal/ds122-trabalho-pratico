<?php
session_start();
require('./banco-dados/credentials.php');
$usuario_logado = $_SESSION['usuario'];

$conn = mysqli_connect($servername,$username,$password,$dbname);

if(!$conn){
    die('problemas ao conectar com o banco de dados' . mysqli_connect_error());
}

$sql = "SELECT l.idliga, l.quantidademaxjogadores, l.quantidadejogadores, l.palavrapasse
FROM liga l
JOIN usuario u ON l.idliga = u.idliga
WHERE u.nome = '$usuario_logado';";

$entrar = mysqli_query($conn, $sql);
$liga = mysqli_fetch_assoc($entrar);

$idliga = $_GET['idliga'] ?? $liga['idliga'] ?? null;


if ($idliga) {
    $sql_verificar_jogada = "SELECT COUNT(*) as jogadas 
                        FROM historico 
                        INNER JOIN usuario ON historico.id_usuario = usuario.id_usuario
                        INNER JOIN partida ON historico.idpartida = partida.idpartida
                        WHERE usuario.nome = '$usuario_logado' 
                            AND partida.id_liga = '$idliga'";
    $result_verificar_jogada = mysqli_query($conn, $sql_verificar_jogada);

    if (!$result_verificar_jogada) {
        die('Problemas ao verificar jogadas' . mysqli_error($conn));
    }

    $jogadas = mysqli_fetch_assoc($result_verificar_jogada);

    if ($jogadas['jogadas'] > 0) {
        header("Location: ligas.php?idliga=$idliga");
        exit();
    }
}

if ($_SERVER["REQUEST_METHOD"] === "POST" && isset($_SESSION['usuario'])) {
    $conn = mysqli_connect($servername, $username, $password, $dbname);
    if (!$conn) {
        die("Error connecting to the database: " . mysqli_connect_error());
    }
        $pontuacao = $_POST["pontuacao"];
        $nomeusuario = $_SESSION['usuario'];
        $email = $_SESSION['usuario'];
        $sql = "SELECT idusuario, email, senha, pontuacaousuario FROM usuario WHERE nome='$nomeusuario' or email='$email'";
        $resultado = mysqli_query($conn, $sql);

    if ($resultado) {
        if (mysqli_num_rows($resultado) > 0) {
            $row = mysqli_fetch_assoc($resultado);
            $idusuario = $row['idusuario'];
            $row['nome'] = $_SESSION['usuario'];
            $email = $row['email'];
            $senha = $row['senha'];
            $pontuacaousuario = $row['pontuacaousuario'];
            $sql = "SELECT idusuario, nome, email FROM usuario WHERE idusuario = $idusuario";
        }
    }

    $data = $_POST['data'];
    $idliga = mysqli_real_escape_string($conn, $idliga);
    $sql = "INSERT INTO partida (data, pontuacao) VALUES ('$data', '$pontuacao')";
    if (mysqli_query($conn, $sql)) {
        $last_insert_id = mysqli_insert_id($conn);
        $sql1 = "INSERT INTO historico (idpartida, idusuario) VALUES ('$last_insert_id', '$idusuario');";
        if (mysqli_query($conn, $sql1)) {
            $sql2 = "SET SQL_SAFE_UPDATES = 0;";
            if (mysqli_query($conn, $sql2)) {
                $sql3 = "UPDATE usuario AS u
                SET u.pontuacaousuario = (
                    SELECT SUM(p.pontuacao) 
                    FROM historico AS h
                    INNER JOIN partida AS p ON h.idpartida = p.idpartida
                    WHERE h.idusuario = '$idusuario'
                )
                WHERE u.idusuario = '$idusuario'";
if (mysqli_query($conn, $sql3)) {
    //echo "Pontuação do usuário atualizada com sucesso!";
} else {
    //echo "Erro ao atualizar a pontuação do usuário: " . mysqli_error($conn);
}

            }
        }
    }

   

    mysqli_close($conn);
}
?>




<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Jogo da digitação</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
    <link rel="stylesheet" href="../css/jogo.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css">
</head>

<body>

    <div class="d-flex align-items-center" style="padding: 20px; padding-left: 80px;" id="top">
        <button class="botao_inicial" type="button" onclick="window.location.href='home.php'">
            <img src="../imgs/duck.png" style="width: 50px; height: 50px; margin-right: 10px;" alt="Home" />
        </button>
        <h1 class="display-6 w-100">
            <span class="nome green">duck</span><span class="nome orange">type</span>
        </h1>
    </div>

    <div id="centro">
        <div id="principal">
            <div id="divpalavra">
                <h2> <span id="palavra"></span></h2>
                <input type="text" id="inputusuario" placeholder="Digite a palavra:">
            </div>
        </div>

        <div id="lado">
            <div id="pontuacao">Pontuação:<span id="score"></span> </div>

            <div id="tempo" style="margin-left: -0.5%;">Tempo restante: <span id="tempoRestanteValor">30</span> segundos
            </div>
            <img src="../imgs/alvopato.png" id="alvopato" style="margin-top:10%; margin-bottom: 20%" alt="quack!">
            <div class="button" id="botao">
                <a href="#">Começar!</a>
            </div>
        </div>
    </div>
    <script src="../js/jogo.js"></script>

</body>

</html>