    <?php
  require('credentials.php');
  // pega o conteúdo do outro arquivo e coloca nesse arquivo, e se algo der errado o require interrompe o script

  // Create connection
  $conn = mysqli_connect($servername, $username, $password);
  // Check connection
  if (!$conn) {
      die("Connection failed: " . mysqli_connect_error());
  }

  // Create database
  $sql = "create database if not exists $dbname";
  if (mysqli_query($conn, $sql)) {
      echo "Database created sucessfully<br>";
  } else {
      echo "Error creating database: " . mysqli_error($conn) . "<br>";
  } 
  
  // Create database
  $sql = "use $dbname";
  if (mysqli_query($conn, $sql)) {
      echo "Database selected successfully<br>";
  } else {
      echo "Error creating database: " . mysqli_error($conn) . "<br>";
  } 


  // sql to create tables
    $sql = "create table if not exists liga (
        idliga int auto_increment primary key,
        nomeliga varchar(50) not null,
        quantidademaxjogadores int,
        quantidadejogadores int,
        palavrapasse varchar(50) not null
    );";

    mysqli_query($conn, $sql);

    $sql = "create table if not exists usuario (
        idusuario int unsigned auto_increment primary key,
        nome varchar(50) not null,
        email varchar(50) not null,
        senha varchar(250) not null,
        pontuacaousuario int,
        idliga int,
        foreign key (idliga) references liga(idliga)
    );"; 

    mysqli_query($conn, $sql);

    $sql = "create table if not exists partida (
        idpartida int unsigned auto_increment primary key,
        pontuacao int,
        data date,
        idliga int,
        idusuario int unsigned, 
        foreign key (idliga) references liga(idliga),
        foreign key (idusuario) references usuario(idusuario)
    );"; 

    mysqli_query($conn, $sql);

    $sql = "create table if not exists historico (
        idpartida int unsigned,
        idusuario int unsigned,
        idliga int,
        jogou boolean DEFAULT 0,
        idhistorico integer unsigned auto_increment primary key,
        foreign key (idpartida) references partida(idpartida),
        foreign key (idusuario) references usuario(idusuario),
        foreign key (idliga) references liga(idliga)
    );"; 

    mysqli_query($conn, $sql);

    $sql = "INSERT INTO 
    liga(nomeliga, quantidademaxjogadores, quantidadejogadores, palavrapasse) 
    VALUES 
    ('agentes patológicos', 4, 0, 'crime');";

    mysqli_query($conn, $sql);

    $sql = "INSERT INTO 
    liga(nomeliga, quantidademaxjogadores, quantidadejogadores, palavrapasse) 
    VALUES
    ('psicopatos', 8, 0, 'assassipato');";

    mysqli_query($conn, $sql);

    $sql = "INSERT INTO 
    liga(nomeliga, quantidademaxjogadores, quantidadejogadores, palavrapasse) 
    VALUES
    ('relampato mcqueen', 3, 0, 'quackchow');";

    mysqli_query($conn, $sql);

    $sql = "INSERT INTO 
    liga(nomeliga, quantidademaxjogadores, quantidadejogadores, palavrapasse) 
    VALUES
    ('digipatos', 5, 0, 'type');";

    mysqli_query($conn, $sql);

    $sql = "INSERT INTO 
    liga(nomeliga, quantidademaxjogadores, quantidadejogadores, palavrapasse) 
    VALUES
    ('quack', 6, 0, 'quack');";

    mysqli_query($conn, $sql);

    $sql = "INSERT INTO
    usuario (nome, email, senha, pontuacaousuario)
    VALUES 
    ('tererere', 'tfrank@gmail.com', 'gatinha', 44);";

    mysqli_query($conn, $sql);

    $sql = "INSERT INTO 
    usuario (nome, email, senha, pontuacaousuario)
    VALUES 
    ('nathileal', 'nathi@gmail.com', 'camaleal', 39);";

    mysqli_query($conn, $sql);

    $sql = "INSERT INTO 
    usuario (nome, email, senha, pontuacaousuario)
    VALUES 
    ('duda kloss', 'ekloss@gmail.com', 'lindona', 31);";

    mysqli_query($conn, $sql);

    $sql = "INSERT INTO 
    usuario (nome, email, senha, pontuacaousuario)
    VALUES 
    ('viol', 'projetoderockeiro@gmail.com', 'gay', 19);";

    mysqli_query($conn, $sql);

    $sql = "INSERT INTO 
    usuario (nome, email, senha, pontuacaousuario)
    VALUES 
    ('andre', 'emodepressivo@gmail.com', 'pareiem2010', 12);";

    mysqli_query($conn, $sql);

    $sql = "INSERT INTO 
    usuario (nome, email, senha, pontuacaousuario)
    VALUES 
    ('pedro', 'jesusfalsificado@gmail.com', 'besourosprateados', 13);";

    mysqli_query($conn, $sql);

    $sql = "INSERT INTO 
    usuario (nome, email, senha, pontuacaousuario)
    VALUES 
    ('doni', 'donissauro@gmail.com', 'emextincao', 25);";

    mysqli_query($conn, $sql);

    $sql = "INSERT INTO 
    usuario (nome, email, senha, pontuacaousuario)
    VALUES 
    ('tads', 'tadsporamor@gmail.com', 'amowebI', 12);";

    mysqli_query($conn, $sql);

    $sql = "INSERT INTO 
    usuario (nome, email, senha, pontuacaousuario)
    VALUES 
    ('donald', 'opato@gmail.com', 'quack', 47);";

    mysqli_query($conn, $sql);

    $sql = "INSERT INTO 
    usuario (nome, email, senha, pontuacaousuario)
    VALUES 
    ('margarida', 'patinha@gmail.com', 'quack', 54);";

    mysqli_query($conn, $sql);

    //tererere 44
    $sql = "INSERT INTO
    partida (pontuacao, data)
    VALUES
    (12, '2023-11-02');";

    mysqli_query($conn, $sql);

    $sql = "INSERT INTO
    partida (pontuacao, data)
    VALUES
    (8, '2023-11-02');";

    mysqli_query($conn, $sql);

    $sql = "INSERT INTO
    partida (pontuacao, data)
    VALUES
    (15, '2023-11-03');";

    mysqli_query($conn, $sql);

    //nathileal 39
    $sql = "INSERT INTO
    partida (pontuacao, data)
    VALUES
    (4, '2023-11-05');";

    mysqli_query($conn, $sql);

    $sql = "INSERT INTO
    partida (pontuacao, data)
    VALUES
    (6, '2023-11-05');";

    mysqli_query($conn, $sql);

    $sql = "INSERT INTO
    partida (pontuacao, data)
    VALUES
    (7, '2023-11-05');";

    mysqli_query($conn, $sql);

    $sql = "INSERT INTO
    partida (pontuacao, data)
    VALUES
    (12, '2023-11-06');";

    mysqli_query($conn, $sql);

    $sql = "INSERT INTO
    partida (pontuacao, data)
    VALUES
    (10, '2023-11-06');";

    mysqli_query($conn, $sql);

    //duda 31
    $sql = "INSERT INTO
    partida (pontuacao, data)
    VALUES
    (10,'2023-11-07');";

    mysqli_query($conn, $sql);

    $sql = "INSERT INTO
    partida (pontuacao, data)
    VALUES
    (10, '2023-11-09');";

    mysqli_query($conn, $sql);

    $sql = "INSERT INTO
    partida (pontuacao, data)
    VALUES
    (11, '2023-11-25');";

    mysqli_query($conn, $sql);

    //viol 9
    $sql = "INSERT INTO
    partida (pontuacao, data)
    VALUES
    (4, '2023-11-27');";

    mysqli_query($conn, $sql);

    $sql = "INSERT INTO
    partida (pontuacao, data)
    VALUES
    (5, '2023-11-27');";

    mysqli_query($conn, $sql);

    $sql = "INSERT INTO
    partida (pontuacao, data)
    VALUES
    (10, '2023-11-28');";

    mysqli_query($conn, $sql);

    //andre 12
    $sql = "INSERT INTO
    partida (pontuacao, data)
    VALUES
    (12, '2023-11-30');";

    mysqli_query($conn, $sql);

    //pedro 13
    $sql = "INSERT INTO
    partida (pontuacao, data)
    VALUES
    (13, '2023-11-30');";

    mysqli_query($conn, $sql);

    //doni 25
    $sql = "INSERT INTO
    partida (pontuacao, data)
    VALUES
    (12, '2023-11-17');";

    mysqli_query($conn, $sql);

    $sql = "INSERT INTO
    partida (pontuacao, data)
    VALUES
    (13, '2023-11-17');";

    mysqli_query($conn, $sql);

    //tads 12
    $sql = "INSERT INTO
    partida (pontuacao, data)
    VALUES
    (12, '2023-11-29');";

    mysqli_query($conn, $sql);

    //donald 47
    $sql = "INSERT INTO
    partida (pontuacao, data)
    VALUES
    (15, '2023-11-15');";

    mysqli_query($conn, $sql);

    $sql = "INSERT INTO
    partida (pontuacao, data)
    VALUES
    (16, '2023-11-16');";

    mysqli_query($conn, $sql);

    $sql = "INSERT INTO
    partida (pontuacao, data)
    VALUES
    (16, '2023-11-16');";

    mysqli_query($conn, $sql);

    //margarida 54
    $sql = "INSERT INTO
    partida (pontuacao, data)
    VALUES
    (17, '2023-11-17');";

    mysqli_query($conn, $sql);

    $sql = "INSERT INTO
    partida (pontuacao, data)
    VALUES
    (18, '2023-11-18');";

    mysqli_query($conn, $sql);

    $sql = "INSERT INTO
    partida (pontuacao, data)
    VALUES
    (19, '2023-11-19');";

    mysqli_query($conn, $sql);

    // to-do: inserts do histórico, ver como pegar os ids

  if (mysqli_query($conn, $sql)) {
      echo "Table  created successfully<br>";
  } else {
      echo "Error creating table: " . mysqli_error($conn) . "<br>";
  }

  mysqli_close($conn);
?> 
