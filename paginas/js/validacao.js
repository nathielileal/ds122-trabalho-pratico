$(document).ready(function() {
  $("#form_cadastro").on("submit", function(event) {
    var nome = $("input[name='form-login-usuario']");
    var email = $("input[name='form-login-usuario']");
    var senha = $("input[name='form-login-senha']");

    // Limpar mensagens de erro
    $("#msg-erro-cadastro").html("");

    if (nome.val() == "" || nome.val() == null) {
      $("#msg-erro-cadastro").html("Obrigatório preencher campo nome");
      event.preventDefault();
      return false;
    }

    if (email.val() == "" || email.val() == null) {
      $("#msg-erro-cadastro").html("Obrigatório preencher campo e-mail");
      event.preventDefault();
      return false;
    }

    if (senha.val() == "" || senha.val() == null) {
      $("#msg-erro-cadastro").html("Obrigatório preencher campo senha");
      event.preventDefault();
      return false;
    }

    return true;
  });

  $("#form-login").on("submit", function(event) {
    var usuario = $("input[name='form-login-usuario']");
    var senha = $("input[name='form-login-senha']");

    $("#msg-erro-login").html("");

    if (usuario.val() == "" || usuario.val() == null) {
      $("#msg-erro-login").html("Obrigatório preencher campo usuário");
      event.preventDefault();
      return false;
    }

    if (senha.val() == "" || senha.val() == null) {
      $("#msg-erro-login").html("Obrigatório preencher campo senha");
      event.preventDefault();
      return false;
    }

    return true;
  });
});

console.log($("#msg-erro-login"));
