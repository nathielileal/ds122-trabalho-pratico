document.addEventListener('DOMContentLoaded', function() {
    let botao = document.getElementById("criar");

    if (botao) {
        botao.addEventListener("click",function(){
            criar_liga();
        });
    }

    carregar_ligas();
})

function criar_liga() {
    let nome_liga = document.getElementById("form-nome-liga").value;

    let qtde_jogadores = document.getElementById("form-qtde-jogadores").value;
        
    var nova_liga = document.createElement("div");
    nova_liga.className = "liga";
            
    nova_liga.innerHTML = `
    <div class="conteudo-liga">
        <div class="informacoes">
            <p class="nome-liga">${nome_liga}</p>
            <div class="qtde-jogadores">
                <p class="jogadores">Jogadores:</p>
                <p class="qtde">${qtde_jogadores}/${qtde_jogadores}</p>
            </div>
        </div>
        <div class="col-md-12 mb-3">
            <button type="button" class="entrar btn btn-outline-custom btn-sm">Entrar</button>
        </div>
    </div>
    `;
        
    document.getElementById("container-liga").appendChild(nova_liga);
}

function carregar_ligas() {
    ligas.forEach(function (liga) {
        criar_liga();
    })
}