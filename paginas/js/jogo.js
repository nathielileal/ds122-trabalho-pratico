document.addEventListener('DOMContentLoaded', function() {
        var botao = document.getElementById('botao');
        var entrada = document.getElementById('inputusuario');
        var palavraescrita = document.getElementById('palavra');
        var tempoRestante = document.getElementById('tempoRestanteValor');
        var scoreSpan = document.getElementById('score');
        var score = 0;
        var tempodisponivel = 30;
        let comecou = false;


        /*window.onload = function() {
            var pontuacao = atualizarpontuacao();
            
            var form = document.createElement("form");
            form.setAttribute("method", "POST");
            form.setAttribute("action", "jogo.php");
    
            var pontuacaoInput = document.createElement("input");
            pontuacaoInput.setAttribute("type", "hidden");
            pontuacaoInput.setAttribute("name", "pontuacao");
            pontuacaoInput.setAttribute("value", pontuacao);
    
            form.appendChild(pontuacaoInput);
            document.body.appendChild(form);
        
            form.submit();
        }; */


        function atualizarpontuacao() {
            scoreSpan.textContent = score;
            tempoRestante.textContent = tempodisponivel;
        }


        function atualizartempo() {
            tempoRestante.textContent = tempodisponivel;
        }



        function exibirPalavraAleatoria() {
            const palavras = ['patinho', 'lerolero', 'laranja', 'contexto', 'hipismo', 'patifaria', 'foca', 'teclado', 'canela', 'pato','bico','pena', 'obsidiana', 'microchip', 'crocodilo', 'aramaico', 'chaleira', 'lontra','quadro','circo','estojo','livro','novembro','pirilampo','vagalume','tecla','exceto','festinha'];
            const indice = Math.floor(Math.random() * palavras.length);
            const palavra = palavras[indice];
            palavraescrita.textContent = palavra;
        }

        function verificarDigitacao() {
            const palavraDigitada = entrada.value.trim().toLowerCase();
            const palavraAtual = palavraescrita.textContent.toLowerCase();

            if (palavraDigitada === palavraAtual) {
                score++;
                entrada.value = '';
                exibirPalavraAleatoria();
                atualizarpontuacao();
            }
        }

        function fimdejogo() {
            comecou = false;
            clearInterval(temporestante);
            palavraescrita.style.display = "none";
            entrada.style.display = "none";
            sendInfoPhp(score);
            reiniciarJogo()
        }

        function sendInfoPhp(pontuacao) {
            function converterParaDataMySQL(data) {
                const dataObj = new Date(data);
                const ano = dataObj.getFullYear();
                const mes = String(dataObj.getMonth() + 1).padStart(2, '0');
                const dia = String(dataObj.getDate()).padStart(2, '0');
              
                const dataMySQL = `${ano}-${mes}-${dia}`;
                return dataMySQL;
              }

            const form = document.createElement("form");
            form.setAttribute("method", "POST");
            form.setAttribute("action", "../php/jogo.php");
        
            const pontuacaoInput = document.createElement("input");
            pontuacaoInput.setAttribute("type", "hidden");
            pontuacaoInput.setAttribute("name", "pontuacao");
            pontuacaoInput.setAttribute("value", pontuacao);
        
            const dataInput = document.createElement("input");
            dataInput.setAttribute("type", "hidden");
            dataInput.setAttribute("name", "data");
            dataInput.setAttribute("value", converterParaDataMySQL(new Date()));
        
            form.appendChild(pontuacaoInput);
            form.appendChild(dataInput);
            document.body.appendChild(form);
        
            form.submit();
        }



        function reiniciarJogo() {
                const reiniciar = confirm("Deseja jogar novamente?");
                if (reiniciar) {
                    palavraescrita.style.display = "none";
                    entrada.style.display = "none";
                    iniciarjogo();
                } else {
                    alert("O jogo acabou. Obrigado por jogar!");
            }
        }

        function iniciarjogo() {
            if (!comecou) {
                comecou = true;
                tempodisponivel = 30;
                score = 0;
                palavraescrita.style.display = "initial";
                entrada.style.display = "block";
                atualizarpontuacao();
                atualizartempo();
                exibirPalavraAleatoria();
                temporestante = setInterval(() => {
                    tempodisponivel--;
                    atualizartempo();
                    if (tempodisponivel === 0) {
                        clearInterval(temporestante);
                        fimdejogo();
                    }
                }, 1000);
            }
            entrada.addEventListener('input', verificarDigitacao);
        }


        botao.addEventListener("click", iniciarjogo);
    });     
  
