# Ducktype - Jogo de Digitação

## Descrição

O projeto Ducktype consiste em uma aplicação web completa que oferece aos usuários um jogo de digitação desafiador e uma plataforma para competições entre jogadores. O objetivo principal é permitir que os jogadores se registrem, participem de partidas de digitação e compitam em várias ligas para alcançar posições de destaque nos quadros de pontuação.

## Funcionamento

Os usuários têm a capacidade de criar uma conta personalizada, realizar login e se envolver em partidas do jogo de digitação. A cada partida, a pontuação é acumulada e exibida de forma individual para cada jogador, além de ser agregada nos quadros de pontuação gerais e específicos das ligas. Os jogadores também têm acesso ao histórico completo de suas partidas, podendo verificar detalhes, incluindo pontuações anteriores e seu desempenho ao longo do tempo.

Além disso, a aplicação permite aos usuários criar suas próprias ligas, bem como se inscrever em ligas existentes. Para o cadastro em uma liga, é necessário o uso de uma palavra-chave específica, determinada pelo criador da liga. As pontuações nessas ligas são exibidas de duas maneiras: pontuação desde a criação da liga e pontuação semanal, incentivando uma competição mais dinâmica e engajadora.

## Tecnologias Utilizadas

O desenvolvimento do sistema foi realizado utilizando diferentes tecnologias. O jogo de digitação foi implementado em JavaScript, enquanto o back-end foi construído com PHP. A interação com o banco de dados MySQL foi essencial para armazenar informações cruciais, como detalhes dos usuários, registros de partidas e pontuações. Além disso, foram aplicadas verificações de campos em JavaScript e PHP para garantir a integridade e segurança dos dados.

## Instruções de Instalação e Execução

Para executar o sistema localmente, é necessário ativar um servidor, como XAMPP ou WAMP. Após baixar o repositório, basta digitar o caminho para a página inicial no navegador. Isso permitirá que os usuários criem contas ou façam login para acessar a página principal ("Home") e desfrutem de todas as funcionalidades do jogo.

## Estrutura de Arquivos

Os arquivos do projeto estão organizados por linguagem. Dentro do repositório "ds-122-trabalho-prático", a pasta "páginas" contém subdiretórios para CSS, imagens (imgs), scripts JavaScript (js) e scripts PHP (php), sendo este último subdividido para lidar especificamente com o banco de dados.

## Licença

Este projeto foi desenvolvido por Maria Tereza Marchezan Frank e Nathieli Leal. Todos os direitos reservados. 

## Considerações finais

O projeto Ducktype  foi desenvolvido com o objetivo de proporcionar uma experiência dinâmica e competitiva para os usuários. Durante o desenvolvimento, nos esforçamos para garantir um sistema funcional e  interativo, seguindo boas práticas de programação e segurança da informação.

Ao longo do processo de criação, enfrentamos desafios significativos, como a integração entre o front-end e o back-end, garantindo a consistência e segurança dos dados dos usuários e a implementação eficaz do jogo de digitação. Superamos esses desafios através de pesquisas e testes, colaboração entre a equipe e dedicação ao projeto.

Por fim, gostaríamos de agradecer ao professor Alexander Kutzke da disciplina Desenvolvimento Web I pelos ensinamentos durante o decorrer da disciplina, conhecimentos esses cruciais para o desenvolvimento do Ducktype.



